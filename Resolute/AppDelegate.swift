//
//  AppDelegate.swift
//  Resolute
//
//  Created by Neal Watkins on 2020/9/16.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        registerDefaults()
        buildButton()
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    let statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.squareLength)
    
    func buildButton() {
        statusItem.button?.image = NSImage(named: "ResoluteIcon")
        statusItem.menu = Menu()
    }
    
    func registerDefaults() {
        let plist = Bundle.main.path(forResource: "Defaults", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: plist!) as? [String: Any] {
            UserDefaults.standard.register(defaults: dic)
        }
    }
    
}



