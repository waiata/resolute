import Cocoa

public extension NSScreen {
    
    /// rect for total area of all screens
    static var extentsOfAllScreens: CGRect {
        screens.reduce(CGRect.zero){ $0.union($1.frame) }
    }
    
    static func screen(id: CGDirectDisplayID?) -> NSScreen? {
        return screens.first(where: { $0.id == id })
    }
    
    private func info(for displayID: CGDirectDisplayID, options: Int) -> [AnyHashable: Any]? {
        var iterator: io_iterator_t = 0
        
        let result = IOServiceGetMatchingServices(kIOMasterPortDefault, IOServiceMatching("IODisplayConnect"), &iterator)
        guard result == kIOReturnSuccess else {
            print("Could not find services for IODisplayConnect: \(result)")
            return nil
        }
        
        var service = IOIteratorNext(iterator)
        while service != 0 {
            let info = IODisplayCreateInfoDictionary(service, IOOptionBits(options)).takeRetainedValue() as! [AnyHashable: Any]
            
            guard
                let vendorID = info[kDisplayVendorID] as! UInt32?,
                let productID = info[kDisplayProductID] as! UInt32?
                else {
                    continue
            }
            
            if vendorID == CGDisplayVendorNumber(displayID) && productID == CGDisplayModelNumber(displayID) {
                return info
            }
            
            service = IOIteratorNext(iterator)
        }
        
        return nil
    }
    
    var index: Int? {
        return NSScreen.screens.firstIndex(of: self)
    }
    
    var id: CGDirectDisplayID {
        deviceDescription[NSDeviceDescriptionKey("NSScreenNumber")] as! CGDirectDisplayID
    }
    
    var name: String {
        if #available(macOS 10.15, *) {
            return localizedName
        }
        
        guard let info = info(for: id, options: kIODisplayOnlyPreferredName) else { return "" }
        
        guard
            let localizedNames = info[kDisplayProductName] as? [String: Any],
            let name = localizedNames.values.first as? String
            else { return "" }
        
        return name
    }
    
    var mode: CGDisplayMode? {
          return CGDisplayCopyDisplayMode(id)
      }
    
    var modes: [CGDisplayMode] {
        guard let list = CGDisplayCopyAllDisplayModes(id, nil) as? [CGDisplayMode] else { return [] }
        return list
    }
    
    var isMain: Bool {
        return CGMainDisplayID() == id
    }
    
    var physicalSize: CGSize {
        return CGDisplayScreenSize(id)
    }
    
    var bounds: CGRect {
        return CGDisplayBounds(id)
    }
    
    var rotation: Double {
        return CGDisplayRotation(id)
    }
    
    var snapshot: CGImage? {
        return CGDisplayCreateImage(id)
    }
    
    var wallpaper: NSImage? {
        guard let url = NSWorkspace.shared.desktopImageURL(for: self) else { return nil }
        let image = NSImage(byReferencing: url)
        guard image.size != .zero else { return nil }
        return image
    }
    
    var backgroundColor: CGColor? {
        guard
            let desk = NSWorkspace.shared.desktopImageOptions(for: self),
            let fill = desk[NSWorkspace.DesktopImageOptionKey.fillColor] as? NSColor
            else { return nil }
        return fill.cgColor
    }
    
    var resolutionLabel: String {
        return "\(Int(bounds.width)) x \(Int(bounds.height))"
    }
}

