//
//  WallpaperItem.swift
//  Resolute
//
//  Created by Neal Watkins on 2020/12/3.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class WallpaperItem: NSMenuItem {
    
    var screen: NSScreen
    
    let workspace = NSWorkspace.shared
    
    var url: URL? {
        get {
            return workspace.desktopImageURL(for: screen)
        }
        set {
            if let url = newValue {
                do {
                    try workspace.setDesktopImageURL(url, for: screen, options: options)
                } catch {
                    print(error.localizedDescription)
                }
            }
            setImage()
        }
    }
    
    var options: [NSWorkspace.DesktopImageOptionKey: Any] {
        return workspace.desktopImageOptions(for: screen) ?? [:]
    }
    
    init(screen: NSScreen) {
        self.screen = screen
        super.init(title: "", action: #selector(chooseWallpaper(_:)), keyEquivalent: "")
        self.target = self
        setImage()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @objc func chooseWallpaper(_ sender: Any?) {
        let panel = NSOpenPanel()
        panel.title = "Choose Wallpaper"
        panel.canChooseDirectories = false
        panel.allowsMultipleSelection = false
        panel.allowedFileTypes = NSImage.imageTypes
        let ok = panel.runModal()
        guard ok == .OK else { return } // cancelled
        url = panel.url
    }
    
    func setImage() {
        guard let url = url else { return }
        let image = NSImage(byReferencing: url)
        image.size = Default.imageSize
        self.image = image
    }
    
    struct Default {
        @Preference(key: "Wallpaper Image Size", def: CGSize(width: 160, height: 90))
        static var imageSize: CGSize
    }
}
