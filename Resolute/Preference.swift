//
//  Preference.swift
//  Connect
//
//  Created by Neal Watkins on 2020/6/19.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Foundation

@propertyWrapper struct Preference<T: Codable> {
    
    var key: String
    var def: T
    var store: UserDefaults = .standard

    var wrappedValue: T {
        get {
            if isStandard {
                return store.value(forKey: key) as? T ?? def
            } else {
                guard let data = store.data(forKey: key) else { return def }
                guard let value = try? PropertyListDecoder().decode(T.self, from: data) else { return def }
                return value
            }
        }
        set {
            if isStandard {
                store.set(newValue, forKey: key)
            } else {
                guard let data = try? PropertyListEncoder().encode(newValue) else { return }
                store.set(data, forKey: key)
            }
        }
    }
    
    /// returns whether T is a standard type for saving to UserDefaults
    var isStandard: Bool {
        let t = def
        return t is NSData || t is NSString || t is NSNumber || t is NSDate || t is NSArray || t is NSDictionary
    }
}

extension Preference where T: ExpressibleByNilLiteral {
    init(key: String, store: UserDefaults = .standard) {
        self.init(key: key, def: nil, store: store)
    }
}
