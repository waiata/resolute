//
//  ModeItem.swift
//  Resolute
//
//  Created by Neal Watkins on 2020/9/16.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class ModeItem: NSMenuItem {

    var display: CGDirectDisplayID
    var mode: CGDisplayMode
    
    /// is this the current display mode
    var isCurrent: Bool {
        return CGDisplayCopyDisplayMode(display) == mode
    }
    
    init(mode: CGDisplayMode, display: CGDirectDisplayID) {
        self.display = display
        self.mode = mode
        super.init(title: mode.label, action: #selector(setMode(_:)), keyEquivalent: "")
        self.target = self
        self.state = isCurrent ? .on : .off
        
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @objc func setMode(_ sender: Any?) {
        /// configure display
        var config: CGDisplayConfigRef?
        CGBeginDisplayConfiguration(&config)
        CGConfigureDisplayWithDisplayMode(config, display, mode, nil)
        CGCompleteDisplayConfiguration(config, .permanently)
        /// set default display
        defaultDisplay = display
        
    }
    
    @Preference(key: "DefaultDisplay")
    var defaultDisplay: CGDirectDisplayID?
    
}
