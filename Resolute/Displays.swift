//
//  Displays.swift
//  Resolute
//
//  Created by Neal Watkins on 2020/9/16.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa
import AVFoundation

struct VideoDevice {
    
    var id: CGDirectDisplayID
    
    init(id: CGDirectDisplayID) {
        self.id = id
    }
    
    var uid: String? {
        //        var address = AudioObjectPropertyAddress.deviceUID
        //
        //        var name: CFString?
        //        var size = UInt32(MemoryLayout<CFString?>.size)
        //        AudioObjectGetPropertyData(self.id, &address, 0, nil, &size, &name)
        //
        //        return name as String?
        return nil
    }
    
    var name: String? {
        //        var address = AudioObjectPropertyAddress.deviceName
        //
        //        var name: CFString?
        //        var size = UInt32(MemoryLayout<CFString?>.size)
        //        AudioObjectGetPropertyData(self.id, &address, 0, nil, &size, &name)
        //
        //        return name as String?
        return nil
    }
    
    var vendorNumber: UInt32 {
        CGDisplayVendorNumber(id)
    }
    var modelNumber: UInt32 {
        return CGDisplayModelNumber(id)
    }
    
    var unitNumber: UInt32 {
        return CGDisplayUnitNumber(id)
    }
    
    var serialNumber: UInt32 {
        return CGDisplaySerialNumber(id)
    }
    
    var physicalSize: CGSize {
        return CGDisplayScreenSize(id)
    }
    
    var bounds: CGRect {
        return CGDisplayBounds(id)
    }
    
    var rotation: Double {
        return CGDisplayRotation(id)
    }
    
    var snapshot: CGImage? {
        return CGDisplayCreateImage(id)
    }
    
    var colorSpace: CGColorSpace {
        return CGDisplayCopyColorSpace(id)
    }
    
    var mode: CGDisplayMode? {
        return CGDisplayCopyDisplayMode(id)
    }
    
    var resolutionLabel: String {
        let bounds = self.bounds
        return "\(bounds.width) x \(bounds.height)"
    }
    
    var modes: [CGDisplayMode] {
        guard let list = CGDisplayCopyAllDisplayModes(id, nil) as? [CGDisplayMode] else { return [] }
        return list
    }
    
    static var all: [VideoDevice] {
        
        /// get number of displays
        let max: UInt32 = 32
        var count: UInt32 = 0
        CGGetActiveDisplayList(max, nil, &count)
        
        /// build list of display IDs
        var list = [CGDirectDisplayID]()
        for _ in 0..<count { list.append(CGDirectDisplayID()) }
        CGGetActiveDisplayList(count, &list, &count)
        
        var devices = [VideoDevice]()
        
        for id in list {
            devices.append(VideoDevice(id: id))
        }
        
        return devices
    }
}

extension VideoDevice: CustomStringConvertible {
    var description: String {
        return "Display [\(id)] Vendor: \(vendorNumber) Model: \(modelNumber) SN: \(serialNumber)"
    }
    
    
}

extension CGDisplayMode: CustomStringConvertible {
    
    public var description: String {
        return "Display Mode (\(ioDisplayModeID)): \(pixelWidth) x \(pixelHeight) @ \(refreshRate)"
    }
    
    var label: String {
        let refresh = (refreshRate == 0) ? "" : " @\(refreshRate)"
        return "\(pixelWidth) x \(pixelHeight)" + refresh
    }
    
    
}
