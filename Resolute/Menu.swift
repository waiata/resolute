//
//  Menu.swift
//  Resolute
//
//  Created by Neal Watkins on 2020/9/16.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class Menu: NSMenu {

    init() {
        super.init(title: "")
        self.delegate = self
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildMenu() {
        for screen in NSScreen.screens {
            build(screen: screen)
        }
        buildBottom()
    }
    
    ///add a separator item to menu unless there's no items already
    func separator() {
        if self.numberOfItems > 0 {
            addItem(NSMenuItem.separator())
        }
    }

    /// add mode items for a given screen
    func build(screen: NSScreen) {
        
        //Separator
        separator()
        
        // Screen Item
        let item = NSMenuItem(title: screen.name, action: nil, keyEquivalent: "")
        self.addItem(item)
        
        //Modes
        for mode in screen.modes.sorted(by: { $0.width < $1.width } ) {
            self.addItem(ModeItem(mode: mode, display: screen.id))
        }
        
        // Wallpaper
        self.addItem(WallpaperItem(screen: screen))
    }
    
    /// quit item
    func buildBottom() {
        separator()
        let quit = NSMenuItem(title: "Quit", action: #selector(NSApplication.terminate(_:)), keyEquivalent: "q")
        self.addItem(quit)
    }

    
}

extension Menu: NSMenuDelegate {
    
    func menuWillOpen(_ menu: NSMenu) {
        
    }
    
    func menuNeedsUpdate(_ menu: NSMenu) {
        self.items = []
        buildMenu()
    }
    
    func menuDidClose(_ menu: NSMenu) {
        
    }
    
    
    
}
